import { ADD_TASK_CLICK, CLICK_ITEM, INPUT_TASK_NAME } from "../constants/task.constans"

export const inputTaskNameAction = (inputValue) => {
    return {
        type: INPUT_TASK_NAME,
        payload: inputValue
    }
}

export const addTaskClickAction = () => {
    return {
        type: ADD_TASK_CLICK
    }
}

export const onClickItemsAction = (taskIndex) => {
    return {
        type: CLICK_ITEM,
        payload: taskIndex
    }
}