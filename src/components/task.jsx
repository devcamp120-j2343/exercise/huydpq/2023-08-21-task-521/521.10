import { Container, Grid, TextField, Button, List, ListItem } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { addTaskClickAction, inputTaskNameAction, onClickItemsAction } from "../action/task.action,"

const Task = () => {
    // khai báo hàm dispatch sự kiện tới redux của store

    const dispatch = useDispatch()
    // useSelector để đọc state từ redux
    const { input, taskList } = useSelector((reduxData) => {
        return reduxData.taskReducer
    })

    const inputChangeHander = (event) => {
        dispatch(inputTaskNameAction(event.target.value))
    }

    const addTaskClickHander = () => {
        dispatch(addTaskClickAction())
    }

    const onClickItems = (index) => {
        dispatch(onClickItemsAction(index))
    }

    return (
        <Container>
            {/* // Grid container là một hàng */}
            <Grid container mt={5} alignItems='center'>
                {/* // Grid item là một cột */}
                <Grid item md={9} >
                    <TextField value={input} id="standard-basic" label="Task" variant="standard" placeholder="subtask" fullWidth onChange={inputChangeHander}/>
                </Grid>
                <Grid item md={3}>
                    <Button variant="outlined" onClick={addTaskClickHander}>Add Task</Button>
                </Grid>
            </Grid>
            <Grid container>
                <List dense={false}>
                    {
                        taskList.map((element, index) => {
                            return (
                                <ListItem key={index} style={{color: element.status ? "green" : "red" }} onClick={()=> onClickItems(index)}>
                                    {index+1} . {element.taskName}
                                </ListItem>
                            )
                        })
                    }
                    
                </List>
            </Grid>
        </Container>
    )
}

export default Task