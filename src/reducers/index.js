// root reducers
import { combineReducers } from "redux"; 

// khai báo các redducer con
import taskReducer from "./taskReducer";

const rootRuducers = combineReducers({
    taskReducer
});

export default rootRuducers