// định nghĩa state khởi tạo cho task

import { ADD_TASK_CLICK, CLICK_ITEM, INPUT_TASK_NAME } from "../constants/task.constans";

const initialState = {
    // input: "teet",
    // taskList: [{
    //     taskName: "task1",
    // }]
    input: "",
    taskList: []
}

const taskReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TASK_NAME:
            state.input = action.payload
            break;
        case ADD_TASK_CLICK:
            state.taskList.push({
                taskName: state.input,
                status: false
            })
            break;
        case CLICK_ITEM:
            const index = action.payload
            state.taskList[index].status = !state.taskList[index].status
            break;
        default:
            break;
    }
    return { ...state };
}

export default taskReducer